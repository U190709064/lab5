public class GCDRec {
    public static void main(String[] args) {
        int x =Integer.parseInt(args[0]);
        int y =Integer.parseInt(args[1]);
        int gcd = gcdRecursively(x,y);
        System.out.println("GCD is " + gcd);
    }
    static int gcdRecursively(int numberOne, int numberTwo) {
        int gcd = numberOne;

        if (numberTwo == 0) {
            return gcd;
        }

        gcd = gcdRecursively(numberTwo, numberOne % numberTwo);
        return gcd;
    }
}
